README
======

## To import the procject in your computer ( just do it once ) 
import the project : 

**git clone https://gitlab.com/pierreblancfat/m1_project.git**


Enter the m1_project diretory : 

**cd m1_project**

add all files to your version control :

**git add --all**

Changer de branch master -> dev ( On developpe sur la branch dev, et quand on a un truc sympa qui marche bien, je ferais un merge sur master) :

**git checkout dev**

## The basic process (to add your code to the server):

If you have created a new file, add it to the git version control : 
**git add pathtofile.ext**

Get the last version of the code (Important to see if something change on the server before you push something):

**git pull** 

Here a conflit can be detected (see Resolve conflict part to solve it)

Then localy save your modification (Do it only if your code compile well) :

**git commit -m "short and explicit message describing the modifications"**

(Not mandatory) Know what's gonna be push :

**git diff --stat --cached origin/dev**

And upload your modification on the server :

**git push**

## Resolve conflict 

First you have to choose a conflict tool, use meld (avaible on the imag' PC). Just need to do it once : 

**git config merge.tool meld**

After when you have a conflict : 

**git mergetool**

It opens meld allowing you to resolve the conclict. When it's done, **commit** and **push**. 



## Run the project

Go on the Bin directory:

**cd Bin/**

Make sure the Bin is empty, if not execute:

**rm -r * -f**

Then, execute cmake and make:

**cmake ../Project/ ; make**

You can now run the .exe files you generated. Example

- **./demo/demo_display_image ../Project/data/blurred_finger.png**

- **./tests/voir_test ../Project/data/blurred_finger.png**

Please don't delete these two examples, in order to everybody can have an example to how to use cmake and the link with opencv.


## Information

Write test for each main functions (unitary tests) and global test (functional tests).
The demo folder is only what we want to present of our project. Like the final code of a part (even if it's not really good to split the code by start / main).
The .exe files should be ONLY in these 2 folders.
Comment the code with Doxygen and in the docs folder let cmake compile the LaTeX.
Use clang to compile (add in Cmake when we know how to do it).

## Naming rules
- files : my_file
- class : MyClass
- variable : my_variable
- function : MyFunction()
- macros : MY_MACRO
- Blocs : { and } separated of the code
