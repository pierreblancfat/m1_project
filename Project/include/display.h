#ifndef DISPLAY_H
#define DISPLAY_H

/*!
* \file display.h
* \brief Display images
*/

#include "imagef.h"


/*! \function CallBackFunc
* \brief Write on the console which pixels have been clicked by the mouse
*
* \param event : int, the mouse event
* \param x: int, the position of the mouse in x-axis
* \param y: int, the position of the mouse in y-axis
* \param flags: int, the operation mode
* \param userdata: void*, User data that is passed as is to the callback.
* It can be used to handle trackbar events without using global variables.
*/
void CallBackFunc(int event, int x, int y, int flags, void *userdata);


/*! \function DisplayMultipleImages
* \brief Displays multiple images of class ImageF in the same window (maximum 12)
*
* \param window_name : string, title of the window displayed
* \param images : vector of string, path to the images to display,
* maximum 11
*/
void DisplayMultipleImages(std::list <class ImageF> images,
                           std::string window_name = "");

class ErrorTooManyImages : public std::exception {
public:
    virtual const char *what(void) const throw () {
        return "[ERROR] Too many images, can only handle 12 images";
    }
};

class ErrorNoImages : public std::exception {
public:
    virtual const char *what(void) const throw () {
        return "[ERROR] No images inserted, can take until 12 images";
    }
};


#endif