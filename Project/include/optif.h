#ifndef OPTIF_H
#define OPTIF_H

/*!
* \file optimization.h
* \brief Image optmization
*/

#include "imagef.h"
#include <math.h>
#include <fstream>
#include <ctime>
#include <eigen3/Eigen/Core>

/*! \class OptiF
* \brief Class allowing to compare two images and to minimise the lost between
* each other.
*/

class OptiF {
private:
    ImageF m_f, m_g; /* Images to compare */

public:
    /*!
    * \brief Constructor of the class
    *
    * \param f : ImageF, reference image
    * \param g : ImageF, image that will be wraped to correspond to f
    */
    OptiF(ImageF f, ImageF g);

    /*!
    * \brief Algorithm evaluating the lost function as the sum of squarred
    * errors between the pixel of the two images or similar as the Pearson correlation coefficient
    * (by default the Pearsonn correlaiton coefficient)
    * 
    * \param p : Eigen::VectorXf, parameters of the warp
    */
    float lost(Eigen::VectorXf p);

    /*!
    * \brief Derivates of the lost function with the sum of squarred
    * 
    * \param x0 : Eigen::VectorXf, parameters of the warp
    * \param h : Eigen::VectorXf, the step of the derivation
    */
    float dlost(Eigen::VectorXf x0, Eigen::VectorXf h);

    /*!
    * \brief Lost function for descents algorithm
    * 
    * \param x0 : Eigen::VectorXf, parameters of the warp
    */
    float lostopti(Eigen::VectorXf p);

    /*!
    * \brief Algorithm finding the best g corresponding to f with a greedy
    * strategy on the x-axis translation
    * 
    * return void
    */
    void greedyX();

    /*!
    * \brief Algorithm finding the best g corresponding to f with a greedy
    * strategy on the y-axis translation
    * 
    * return void
    */
    void greedyY();

    /*!
    * \brief Algorithm finding the best g corresponding to f with a greedy
    * strategy on the translation transformation only
    * 
    * return void
    */
    void greedyXY();


    /*!
    * \brief Algorithm finding the best g corresponding to f with a greedy
    * strategy
    * 
    * return void
    */
    void AllGreedy();

    /*!
    * \brief Algorithm finding the best g corresponding to f with a greedy
    * strategy acrosing all the parameters possible (in translation and rotation)
    * 
    * return void
    */
    void AllTrueGreedy();

    /*!
    * \brief Local optimisation Euristic finding the argmin of the lost function. Explore the neigbours of the lost
    * function at the inital point, and goes to the the minimal one. If the miminal value is the current points, the
    * step is diminishing of one half
    *
    * \param x0 Initial point of the euristic
    * \param alpha Step scale factor
    * \param iter_limit Maximum iterations number
    */
    int  NonDifferentiableOptiDescent(Eigen::VectorXf x0, float alpha = 0.1, int iter_limit = 200);



    /*!
    * \brief Local optimisation algorithm finding the argmin of the lost function. From the initial points x0, goes to the opposite
    * of the gradient of lost(x0), with a gamma step scale factor at the next iteration, and so one until the step is less than
    * the precision, or until the numbers of iteration is greater than max_iter
    *
    * \param x0 Initial point of the euristic
    * \param alpha Step scale factor
    * \param iter_limit Maximum iterations number
    */
    void GradientDescent(Eigen::VectorXf x0, Eigen::VectorXf pas,
                         float gamma = 0.01, float precision = 0.01, int max_iter = 1000);



    /*!
    * \brief Return a Eigen vector containing the gradient of the lost function at x, with forwad finite difference with h step
    * \param x : VectorXf, Point of evaluation
    * \param h : VectorXf, Step of finite difference
    */
    Eigen::VectorXf DLostForwad(Eigen::VectorXf x, Eigen::VectorXf h);


    void plotXY();

    /*!
    * \brief Create a plotfile at "path" destinated to be fill with data
    * 
    */
    void Create_plotfile(std::string path, std::ofstream &plotfile);
};

#endif
