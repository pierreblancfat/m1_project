#ifndef UTILS_BLUR_H
#define UTILS_BLUR_H

#include "imagef.h"
#include "utils_pressure.h"

/*! \brief Convolution between two matrices 
*
* \param matrix_1 : Mat, first matrix to convolve
* \param matrix_2 : Mat, second matrix to convolve
* \param output : Mat, matrix containing the convolution
*/
void ConvolutionMatrices(cv::Mat matrix_1, cv::Mat matrix_2, cv::Mat &output);

/*! \brief FFT of an image
*
* \param input : MatrixXf, matrix to apply the FFT
* \param output : Mat, matrix to store the FFT
*/
void FastFourierTransform(Eigen::MatrixXf input, cv::Mat &output_mat);

/*! \brief Inverse FFT of an image
*
* \param input : Mat, matrix to apply the inverse of the FFT
* \param output : MatrixXf, matrix to store the inverse of the FFT
*/
void InverseFastFourierTransform(cv::Mat input, Eigen::MatrixXf &output);

/*! \brief Convolution between two matrices with the FFT formula
*
* \param image : MatrixXf, image
* \param kernel : MatrixXf, kernel
* \param output : MatrixXf, output corresponding to the convolution
*/
void ConvolutionFFT(cv::Mat &image, cv::Mat &kernel, 
cv::Mat &output);

/*! \brief Blur with an averaging kernel of size 7*7
*
* \param image : ImageF, image to blur
* \param im_output : ImageF, in which to store the blurred image
* \param : path_save : string, where to save the blurred image */
void BoxBlur(ImageF image, ImageF im_output, std::string path_save);

/*! \brief Blur with an gaussian kernel of size 7*7
*
* \param image : ImageF, image to blur
* \param im_output : ImageF, in which to store the blurred image
* \param : path_save : string, path where to save the blurred image */
void GaussianBlur(ImageF image, ImageF im_output, std::string path_save);

/*! \brief Adaptative kernel of size 7x7 with energy decrease
*
* \param image : ImageF, image to blur
* \param im_output : ImageF, in which to store the blurred image
* \param center : Point, center of the image
* \param : path_save : string, path where to save the blurred image */
void AdaptativeBlur1(ImageF image, ImageF im_output, cv::Point center, 
std::string path_save);

/*! \brief Adaptative kernel of size 7x7 with constant energy
*
* \param image : ImageF, image to blur
* \param im_output : ImageF, in which to store the blurred image
* \param center : Point, center of the image
* \param : path_save : string, path where to save the blurred image */
void AdaptativeBlur2(ImageF image, ImageF im_output, cv::Point center, 
std::string path_save);

#endif