#ifndef UTILS_PRESSURE_H
#define UTILS_PRESSURE_H

#include "imagef.h"

/*! \brief Thresholds the image to delete the non iformation parts of
an image
*
* \param threshold : int, value over which to put a pixel to white
* \param path_image : path to the image to threshold
* \param path_save : path to where the image is to be saved
*/
void Threshold(float threshold, std::string path_image, std::string path_save);
/*! \brief Looks for the center of the image
*
* \param path_image : string, path to the image
* \param characteristics : MatrixXf, where to store the characteristics
* of the center
*/
void ImageCenter(std::string path_image, Eigen::MatrixXf &characteristics);
/*! \brief Simulation of a weak pressure with the isotropic function
g(x,y) = 1 - (1 - f(x,y)) * exp(-ri/nb_rows), ri the euclidian distance
*
* \param path_image : string, pat to image to modify
* \param path_save : string, path where to save the image
* \param center : Point, center of the image
* \param pixel_values : MatrixXf, where the new pixel values are stored
*/
void IsotropicWeakPressure(std::string path_image, std::string path_save,
                           cv::Point center, Eigen::MatrixXf &pixel_values);
/*! \brief Simulation of a weak pressure with the isotropic function
g(x,y) = 1 - (1 - f(x,y)) * exp(-ra/nb_rows), ra the Mahalanobis distance
*
* \param path_image : string, pat to image to modify
* \param path_save : string, path where to save the image
* \param center : Point, center of the image
* \param big_diameter : float, big diameter of the ellipse
* \param small_diameter : float, small diameter of the ellipse
* \return A vector with the new pixel values
* \param pixel_values : MatrixXf, where the new pixel values are stored
*/
void AnisotropicWeakPressure(std::string path_image, std::string path_save,
                             cv::Point center, float big_diameter, float small_diameter,
                             Eigen::MatrixXf pixel_values);

#endif