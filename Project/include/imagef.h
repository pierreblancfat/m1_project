#ifndef IMAGEF_H
#define IMAGEF_H

/*!
* \file imagef.h
* \brief Image manipulation
*/
#include <string>
#include <iostream>
#include <list>
#include <cmath>
#include <cstdlib>   /* required by atoi, EXIT_FAILURE, EXIT_SUCCESS */
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>
#include <opencv2/core.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/mat.hpp>
#include "display.h"


/*! \class ImageF
* \brief Class representing a fingerprint image, its characteristics and
* some basics manipulations.
*/

class ImageF {

private:
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> m_imgf; /*!< Image */
    unsigned char m_type; /* Output type of the fingerprint image */
    std::string m_path; /* Save the path of the image */

public:

    /*! \friend function DisplayMultipleImages
    * \brief Displays multiple images of class ImageF in the same window
    * (maximum 12)
    *
    * \param images : vector of string, path to the images to display,
    * maximum 11
    */
    friend void DisplayMultipleImages(std::list <class ImageF> images,
                                      std::string window_name);


    /*!
    * \brief Empty Constructor of the class
    */
    ImageF(const unsigned int &height, const unsigned int &width);
    /*!
    * \brief Eigen Matrix constructor
    */
    ImageF(Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> mat);
    /*!
    * \brief Copy Cons
    */
    ImageF(const ImageF &im);

    /*!
    * \brief Constructor of the class
    * \param path_image : string, path of the image
    * \param conver_float : bool, convert the image in float, by default yes
    */
    ImageF(std::string path_image, bool convert_float = true);

    /*! \brief Displays the image, as well as its characteristics
    * \param window_name : string, title of the window displayed
    */
    void DisplayImage(std::string window_name = "") const;

    /*! \brief Looks for the value of the pixel with minimum intensity
    *
    * \return the value of the minimum
    */
    float Minimum() const;

    /*! \brief Looks for the value of the pixel with maximum intensity
    *
    * \return the value of the maximum
    */
    float Maximum() const;

    /*! \brief Looks for the pixels with the minimum intensity and finds their center
    *
    * \return the center of the points where the minimum is reached
    */
    cv::Point AreaMinimumIntensity();

    /*! \brief Looks for the pixel with the maximum intensity and finds their center
    *
    * \return the center of the points where the maximum is reached
    */
    cv::Point AreaMaximumIntensity();

    /*! \brief Convert the output type of the image
    */
    void Convert(unsigned char type);

    /*! \brief Change the value of a pixel
    *
    * \param Point : Point, position of the pixel
    * \param value : float, new value of the pixel
    */
    void ChangeValue(cv::Point pixel, float value);

    /*! \brief Creates a white patch and a black patch on an image
    *
    * \param pixel : Point, position of the top-left pixel of the rectangle
    * \param height : unsigned int, height of the rectangle
    * \param width : unsigned int, width of the rectangle
    * \param value : float, new value inside the rectangle
    */
    void CreateRectangle(cv::Point pixel, unsigned int height,
                         unsigned int width, float value);

    /*! \brief Performs the symmetry of an image along an axis
    *
    * \param axis : unsigned int, to choose along which axis to do the
    * symmetry: 1 for x-axis and 2 for y-axis
    */
    void AxisSymmetry(unsigned int axis);

    /*! \brief Performs the symmetry of an image along the diagonal
    */
    void DiagonalSymmetry();

    /*! \brief Save the image in path
    *
    * \param path : string path of the saved image
    */
    void Save(std::string path);

    /*! \brief Save a symmetric image along an axis
    *
    * \param axis : unsigned int, to choose along which axis to do the
    * symmetry: 1 for x-axis and 2 for y-axis
    * \param path : string, path of the saved image
    */
    void SaveAxisSymmetry(unsigned int axis, std::string path);

    /*! \brief Save a symmetric image along the diagonal
    *
    * \param path : string, path of the saved image
    */
    void SaveDiagonalSymmetry(std::string path);

    /*! \brief Creation of a fingerprint with weak pressure starting from
    an image, with an isotropic function, applied to the whole image
    *
    * \param path_image : string, path to the image we want to modify
    * \param center : Point, where the intensity is maximal on the opened image
    * \param path_save : string, where to save the image with weak pressure
    * \return a vector of floats containing the new values of the pixels
    */
    std::vector<float> WeakPressure1(std::string path_image, cv::Point center,
                                     std::string path_save);

    /*! \brief Creation of a fingerprint with weak pressure starting from
    an image, with an anisotropic function, applied to the whole image
    *
    * \param path_image : string, path to the image we want to modify
    * \param center : Point, where the intensity is maximal on the opened image
    * \param path_save : string, where to save the image with weak pressure
    * \return a vector of floats containing the new values of the pixels
    */
    std::vector<float> WeakPressure2(std::string path_image, cv::Point center,
                                     std::string path_save);

    /*! \brief translate the image in x or y direction, negative number
    * rigth or bottom translation, positive number left or top translation
    *
    * \param x : int, mooves the images of x pixel in x-axis
    * \param y : int, mooves the images of y pixel in y-axis
    * \return void
    */
    void Translation(float x, float y);

    /*! \brief Rotate the image with a given angle
    *
    * \param r : float, rotation angle of the image
    * \return void
    */
    void Rotation(float r);

    /*! \brief Interpolate coordinates pixel for a transformation of an image
    * by default we chose the bilinear interpolation
    *
    * \param v : Vector3f, coordinates of a pixel
    * \return float
    */
    float Interpolate(Eigen::Vector3f v);

    /*! \brief Transform the image according some motion model and pre-set
    * with the given parameters
    *
    * \param p : Eigen::VectorXf, parameters set with a two floats for the
    * translation and a float angle for the rotation
    * \return void
    */
    void Warp(Eigen::VectorXf p);

    /*! \brief Transform the image according some motion model and pre-set
    * with the given parameters and using OpenCV library
    *
    * \param x : float, translation in x-axis
    * \param y : float, translation in y-axis
    * \param angle : float, rotation of angle r
    * \return Eigen::Matrix
    */
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>   WarpOpenCV(float x,
            float y, float angle);


    /* Getters */

    /*! \brief Get the height of the image
    *
    * \return the height of the image (number of rows)
    */

    unsigned int GetHeight();

    /*! \brief Get the width of the image
    *
    * \return the width of the image (number of columns)*/
    unsigned int GetWidth();

    /*! \brief Get the type of the image
    *
    * \return the type of the image with data type unsigned char */
    unsigned char GetType();


    /*! \brief Getter on eigen matrix   */
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> *GetMatrix();

    /*! \brief Getter on initial path   */
    std::string GetPath();

    float GetValue(int i, int j);


    /* Exceptions */
    class ErrorImageNotFound : public std::exception {
    public:
        virtual const char *what(void) const throw () {
            return "[ERROR] Image not found";
        }
    };

    class ErrorTypeUnknown : public std::exception {
    public:
        virtual const char *what(void) const throw () {
            return "[ERROR] Type unknown: != CV_8U || CV_16U || CV_16S \
|| CV_32S || CV_32F || CV_64F ";
        }
    };

    class ErrorOutRange : public std::exception {
    public:
        virtual const char *what(void) const throw () {
            return "[ERROR] Out image range";
        }
    };

    class ErrorOverScale : public std::exception {
    public:
        virtual const char *what(void) const throw () {
            return "[ERROR] Over image scale";
        }
    };

    class ErrorSymmetryInput : public std::exception {
    public:
        virtual const char *what(void) const throw () {
            return "[ERROR] Input for symmetry can only be 1 (x-axis) \
                    or 2 (y-axis)";
        }
    };

};

#endif
