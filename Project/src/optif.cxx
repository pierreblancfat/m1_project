#include "optif.h"


using namespace std;
using namespace cv;
using namespace Eigen;

OptiF::OptiF(ImageF f, ImageF g):m_f(f), m_g(g)
{
}

float OptiF::lost(VectorXf p)
{
    ImageF img_warped(m_g);
    img_warped.Warp(p);

    // lost1: sum of square
    return  powf((((*m_f.GetMatrix() - *img_warped.GetMatrix()).norm())),2);

    // // lost2: pearson corelation coefficient
    // MatrixXf mean_f = MatrixXf::Ones(m_f.GetHeight(), m_f.GetWidth())
    //     * (*m_f.GetMatrix()).mean();
    // MatrixXf mean_img_warped = MatrixXf::Ones(img_warped.GetHeight(), img_warped.GetWidth())
    //     * (*img_warped.GetMatrix()).mean();
    // float top = ((*m_f.GetMatrix() - mean_f).array() * (*img_warped.GetMatrix() - mean_img_warped).array()).sum();
    // float bot = (*m_f.GetMatrix() - mean_f).norm() * (*img_warped.GetMatrix() - mean_img_warped).norm();
    // return  top/bot;
}


float OptiF::lostopti(VectorXf p) {
    return  powf((((*m_f.GetMatrix() - m_g.WarpOpenCV(p[0], p[1], p[2])).norm())),
                 2);
}

float OptiF::dlost( VectorXf x0, VectorXf h)
{
    return this->lostopti(x0+h)- this->lostopti(x0);
}  


void OptiF::greedyX(){
    float mini = 0;
    VectorXf p(3);
    VectorXf p2(3);
    //plotfile
    ofstream plotfile;
    std::time_t now = std::time(NULL);
    std::tm * ptm = std::localtime(&now);
    char buffer[32];
    std::strftime(buffer, 32, "%Y-%m-%d", ptm);
    plotfile.open(string("./tests/system_testing/plots/plot_opti_greedyX_")+string(buffer)+string(".dat"));
    plotfile.clear();
    plotfile << "#Translation-x Error" << endl;
    for (float i = -float(m_f.GetWidth()); i < float(m_f.GetWidth()); i+=float(0.1)){
        p << i,0,0;
        plotfile << i << " " << lost(p) << endl;
        p2 << mini,p(1),p(2);
        if(lost(p)<lost(p2))
        {
            mini = p(0);
        }
    }

    cout << mini << endl;
}

void OptiF::greedyY(){
    float minj = 0;
    VectorXf p(3);
    VectorXf p2(3);
    //plotfile
    ofstream plotfile;
    std::time_t now = std::time(NULL);
    std::tm * ptm = std::localtime(&now);
    char buffer[32];
    std::strftime(buffer, 32, "%Y-%m-%d", ptm);
    plotfile.open(string("./tests/system_testing/plots/plot_opti_greedyY_")+string(buffer)+string(".dat"));
    plotfile.clear();
    plotfile << "#Translation-y Error" << endl;
    for (float j = -float(m_f.GetHeight()); j < float(m_f.GetHeight()); j+=float(0.1)){
        p << 0,j,0;
        plotfile << j << "   " << lost(p) << endl;
        p2 << p(0),minj,p(2);
        if(lost(p)<lost(p2))
        {
            minj = p(1);
        }
    }

    cout << minj << endl;
}


void OptiF::greedyXY(){
    float mini = 0;
    float minj = 0;
    VectorXf p(3);
    VectorXf p2(3);
    //plotfile
    ofstream plotfile;
    std::time_t now = std::time(NULL);
    std::tm * ptm = std::localtime(&now);
    char buffer[32];
    std::strftime(buffer, 32, "%Y-%m-%d", ptm);
    plotfile.open(string("./tests/system_testing/plots/plot_opti_greedyXY_")+string(buffer)+string(".dat"));
    plotfile.clear();
    plotfile << "#Translation-x Translation-y Error" << endl;
    for(unsigned int round = 0; round < 3; round++)
    {
        for (float i = -float(m_f.GetWidth()); i < float(m_f.GetWidth()); i+=float(1)){
            p << i,minj,0;
            plotfile << p(0) << "   " << p(1) << "   " << lost(p) << endl;
            plotfile << p(0) << "   " << p(1) << "   " << lost(p) << endl;
            p2 << mini,p(1),p(2);
            if(lost(p)<lost(p2))
            {
                mini = p(0);
            }
        }

        for (float j = -float(m_f.GetHeight()); j < float(m_f.GetHeight()); j+=float(1)){
            p << mini,j,0;
            plotfile << p(0) << "   " << p(1) << "   " << lost(p) << endl;
            p2 << p(0),minj,p(2);
            if(lost(p)<lost(p2))
            {
                minj = p(1);
            }
        }
    }
    cout << mini << " " << minj << endl;
}



void OptiF::AllTrueGreedy()
{
    float mini = 0;
    float minj = 0;
    float minr = 0;
    float l = 0;
    VectorXf p(3);
    VectorXf p_temp(3);
    //plotfile
    ofstream plotfile;
    std::time_t now = std::time(NULL);
    std::tm * ptm = std::localtime(&now);
    char buffer[32];
    std::strftime(buffer, 32, "%Y-%m-%d", ptm);
    plotfile.open(string("./tests/system_testing/plots/plot_opti_AllTrueGreedy_")+string(buffer)+string(".dat"));
    plotfile.clear();
    plotfile << "#Translation-x Translation-y Rotation-r Error" << endl;
    for (float i = -float(150); i < float(150); i+=float(1))
    {
        for (float j = -float(150); j < float(150); j+=float(1))
        {
            for(float r = float(0); r < float(360); r+=float(1))
            {

                p << i,j,r;
                plotfile << p(0) << "   " << p(1) << "   " << p(2) << "   " 
                    << lost(p) << endl;

                l = lost(p);
                p_temp << p(0),p(1),minr;
                if(l<lost(p_temp))
                {
                    minr = p(2);
                }
                p_temp << mini,p(1),p(2);
                if(l<lost(p_temp))
                {
                    mini = p(0);
                }
                p_temp << p(0),minj,p(2);
                if(l<lost(p_temp))
                {
                    minj = p(1);
                }
            }
        }
    }
    cout << mini << " " << minj << " " << minr << endl;
}


void OptiF::AllGreedy()
{
    float mini = 0;
    float minj = 0;
    float minr = 0;
    VectorXf p(3);
    VectorXf p2(3);
    //plotfile
    ofstream plotfile;
    std::time_t now = std::time(NULL);
    std::tm * ptm = std::localtime(&now);
    char buffer[32];
    std::strftime(buffer, 32, "%Y-%m-%d", ptm);
    plotfile.open(string("./tests/system_testing/plots/plot_opti_AllGreedy_")+string(buffer)+string(".dat"));
    plotfile.clear();
    plotfile << "#Translation-x Translation-y Rotation-r Error" << endl;
    for(unsigned int round = 0; round < 3; round++)
    {
        for (float i = -float(m_f.GetWidth()); i < float(m_f.GetWidth()); i+=float(1)){
            p << i,minj,minr;
            plotfile << p(0) << "   " << p(1) << "   " << p(2) << "   " 
                << lost(p) << endl;
            p2 << mini,p(1),p(2);
            if(lost(p)<lost(p2))
            {
                mini = p(0);
            }
        }

        for (float j = -float(m_f.GetHeight()); j < float(m_f.GetHeight()); j+=float(1)){
            p << mini,j,minr;
            plotfile << p(0) << "   " << p(1) << "   " << p(2) << "   " 
                << lost(p) << endl;
            p2 << p(0),minj,p(2);
            if(lost(p)<lost(p2))
            {
                minj = p(1);
            }
        }

        for (float r = -float(M_PI); r < float(M_PI); r+=float(M_PI/360)){
            p << mini,minj,r;
            plotfile << p(0) << "   " << p(1) << "   " << p(2) << "   " 
                << lost(p) << endl;
            p2 << p(0),p(1),minr;
            if(lost(p)<lost(p2))
            {
                minr = p(2);
            }
        }
            cout << mini << " " << minj << " " << minr << endl;
    }

}


int OptiF::NonDifferentiableOptiDescent(Eigen::VectorXf x0, float alpha,
                                         int iter_limit) {
    //Declaration
    int argmin[3];
    float min, res, bx = 0, by = 0, ba = 0;
    ofstream plotfile;
    VectorXf p(3);
    //plotfile
    this->Create_plotfile(string("./tests/system_testing/plots/plot_opti_non.dat"),
                          plotfile);
    //heuristic
    int cpt = 0;
    res = std::numeric_limits<float>::max();

    while(res > 0 && cpt < iter_limit) {
        min = std::numeric_limits<float>::max();

        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                for (int rot = 0; rot < 3; rot++) {
                    //TODO better memory
                    p << x0[0] + alpha *x0[0]*(x - 1),
                    x0[1] + alpha *x0[1]*(y - 1),
                    x0[2] + alpha *x0[2]*(rot - 1);

                    res = lostopti(p);
                    if( res < min) {
                        min = res;
                        argmin[0] = x;
                        argmin[1] = y;
                        argmin[2] = rot;
                        bx = x0[0] + (alpha * x0[0]) * (x - 1);
                        by = x0[1] + (alpha * x0[1]) * (y - 1);
                        ba = x0[2] + (alpha * x0[2]) * (rot - 1);
                    }

                }
            }
        }

        if (argmin[0] == 1 && argmin[1] == 1
                && argmin[2] == 1 ) { //change not accepted, diminishe the step
            alpha = alpha * 0.5;
        } else { //Change accepted, increase the step
            alpha = alpha * 1.1;
            x0[0] =  bx;
            x0[1] =  by;
            x0[2] =  ba;
            plotfile << " " <<  cpt << " " << res << endl;
            cpt++;
        }
    }

    cout << "x :"<<x0[0] << ", y : " << x0[1] << ", angle : " << x0[2] << endl;
    cout << " res :" << res << " iteration number:" << cpt << endl;
    return 1;
}


void OptiF::GradientDescent(VectorXf x0, VectorXf pas, float gamma,
                            float precision, int max_iter ) {
    int iters = 0;
    int dim = x0.size();
    float step_size = precision + 1;
    VectorXf prev_x(dim);
    ofstream plotfile;
    //plotfile creattion
    this->Create_plotfile(string("tests/system_testing/plots/plot_opti_gradient"),
                          plotfile);

    //Gradient descent
    while(iters < max_iter && step_size > precision) {
        prev_x = x0;
        //Descent
        x0 -=  DLostForwad(prev_x, pas) * gamma;
        //Step size
        step_size = (x0 - prev_x).norm();
        iters++;
        //Write in plotfile
        plotfile << " " <<  iters << " " << this->lost(x0) << endl;
    }

    cout << x0 << endl;
    plotfile.close();
}

VectorXf  OptiF::DLostForwad(VectorXf x, VectorXf h) {
    VectorXf dx(x.size()),  hd(x.size());
    hd.setZero();

    // For each dimension
    for(int i = 0; i < x.size(); i++) {
        hd[i] = h[i];
        //Partial derivative with finite differences
        dx[i] = dlost(x, hd) / (hd[i]);
        hd.setZero();
    }

    return dx;
}

void OptiF::plotXY()
{
    VectorXf p(3);
    //plotfile
    ofstream plotfile;
    std::time_t now = std::time(NULL);
    std::tm * ptm = std::localtime(&now);
    char buffer[32];
    std::strftime(buffer, 32, "%Y-%m-%d", ptm);
    plotfile.open(string("./tests/system_testing/plots/plot_XY.dat"));
    plotfile.clear();
    plotfile << "# X Y lost" << endl;
    for (float i = -float(m_f.GetWidth()); i < float(m_f.GetWidth()); i++){
        for (float j = -float(m_f.GetHeight()); j < float(m_f.GetHeight()); j++){
        p << i,j,0;
        plotfile << i << " "<< j << " " << lost(p) << endl << endl;
        // plotfile << i << " " << lost(p) << endl;
        }
    }
}


void OptiF::Create_plotfile(string path, ofstream &plotfile){
    std::time_t now = std::time(NULL);
    std::tm * ptm = std::localtime(&now);
    char buffer[32];
    std::strftime(buffer, 32, "%d-%m-%Y_%H:%M:%S", ptm);
    plotfile.open(path);
    plotfile.clear();
}