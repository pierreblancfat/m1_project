#include "utils_pressure.h"

using namespace std;
using namespace cv;
using namespace Eigen;


void Threshold(float threshold, string path_image, string path_save) {
    /* Getting the image to threshold */
    ImageF image(path_image);

    for(int i = 0; i < image.GetHeight(); i++) {
        for(int j = 0; j < image.GetWidth(); j++) {
            /* Getting the (i,j) pixel and comparing it to the threshold */
            MatrixXf *img = image.GetMatrix();

            if((*img)(i, j) > threshold) {
                //cout << "before " << img(i,j) << endl;
                /* Setting the pixel to white */
                (*img)(i, j) = 1;
                //cout << "after " << img(i,j) << endl;
            }
        }
    }

    /* Saving the modified image */
    image.Save(path_save);

}

void ImageCenter(string path_image, MatrixXf &characteristics) {
    /* Opening the image */
    ImageF image(path_image);
    MatrixXf *img = image.GetMatrix();
    /* Resizing our matrix to put our eleements */
    characteristics.resize(1, 4);
    int i = 0;
    int j = 0;

    /* Going through the image until we find the first black pixel */
    while((*img)(i, j) > 0.3) {
        i++;
        j++;
    }

    /* Going through the column until we find the first black pixel starting
    from the last row */
    int n = (*img).rows() - 1;
    int col = j;

    while((*img)(n, col) > 0.3) {
        n--;
    }

    /* Computing of the distance between the first black pixel and the last one
    in the same column */
    float height = n - i;
    /* Computing the index of the row on which the center is */
    int row = int(i + height / 2);
    int k = 0;
    int l = (*img).cols() - 1;

    /* Giong through the row to get the first and last black pixel */
    while((*img)(row, k) > 0.3) {
        k++;
    }

    while((*img)(row, l) > 0.3) {
        l--;
    }

    /* Computing of the distance between the first black pixel and the last one
    in the same row */
    float width = l - k;
    /* Filling the matrix */
    characteristics(0, 0) = row;
    /* For all images, as we threshold them, we are less precise on the place of
    the first balck pixel we encounter, so on the column where the center is,
    it's why we add 50 to our column number for the center */
    characteristics(0, 1) = col + 50;
    characteristics(0, 2) = height;
    characteristics(0, 3) = width;
}

void IsotropicWeakPressure(string path_image, string path_save,
                           Point center, MatrixXf &pixel_values) {
    ImageF weak_pressure(path_image);
    MatrixXf *img = weak_pressure.GetMatrix();
    /* Resizing the matrix to store the new pixel values */
    pixel_values.resize((*img).rows(), (*img).cols());

    for(int i = 0; i < (*img).rows(); i++) {
        for(int j = 0; j < (*img).cols(); j++) {
            /* Creating the point corresponding to the indices (i,j) */
            Point R(i, j);
            /* Calculating the norm */
            float r = norm(center - R);
            /* Calculating the new value of the pixel */
            float new_value = 1 - (1 - (*img)(i, j)) * \
                              exp(-r / (*img).rows());
            /* Changing its value in the image */
            weak_pressure.ChangeValue(R, new_value);
            /* Adding the new pixel value to the vector */
            pixel_values(i, j) = new_value;
        }
    }

    /* Saving the modified image */
    weak_pressure.Save(path_save);
}

void AnisotropicWeakPressure(string path_image, string path_save,
                             cv::Point center, float big_diameter, float small_diameter,
                             MatrixXf pixel_values) {
    ImageF weak_pressure(path_image);
    MatrixXf *img = weak_pressure.GetMatrix();
    /* Resizing the matrix to store the new pixel values */
    pixel_values.resize((*img).rows(), (*img).cols());

    for(int i = 0; i < (*img).rows(); i++) {
        for(int j = 0; j < (*img).cols(); j++) {
            /* Creating the point corresponding to the indices (i,j) */
            Point R(i, j);
            /* Calculating the distance */
            float r = sqrt(pow((i - center.x) / big_diameter, 2) + \
                           pow((j - center.y) / small_diameter, 2));
            /* Calculating the new value of the pixel */
            float new_value = 1 - (1 - (*img)(i, j)) * \
                              exp(-r);
            /* Changing its value in the image */
            weak_pressure.ChangeValue(R, new_value);
            /* Adding the new pixel value to the vector */
            pixel_values(i, j) = new_value;
        }
    }

    /* Saving the modified image */
    weak_pressure.Save(path_save);
}