#include "utils_blur.h"

using namespace std;
using namespace cv;
using namespace Eigen;

void ConvolutionMatrices(Mat matrix_1, Mat matrix_2, Mat &output) {
    /* Getting the matrices formats */
    int n = matrix_1.rows;
    int m = matrix_1.cols;
    int p = matrix_2.rows;
    int q = matrix_2.cols;
    /* Initialization of the output matrix */
    output = Mat::zeros(n, m, CV_32F);
    /* Getting the center of the second matrix */
    int matrix_2_centerx = q / 2;
    int matrix_2_centery = p / 2;

    /* Going through matrix 1 */
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < m; ++j) {
            /* Going through matrix 2 */
            for(int k = 0; k < p; ++k) {
                /* keeping the row index to convolve */
                int row_index = p - 1 - k;

                for(int l = 0; l < q; ++l) {
                    /* keeping the column index to convolve */
                    int column_index = q - 1 - l;
                    int new_i = i + (matrix_2_centery - k);
                    int new_j  = j + (matrix_2_centerx - l);

                    /* Ignoring out of bounds coefficients */
                    if(new_i >= 0 && new_i < n && new_j >= 0 && new_j < m) {
                        output.at<float>(i, j) = output.at<float>(i, j) + \
                                                 matrix_1.at<float>(new_i, new_j) * matrix_2.at<float>(k, l);
                    }
                }
            }
        }
    }
}

void FastFourierTransform(MatrixXf input, Mat &output_mat) {
    /* Convertion from MatrixXf to Mat */
    Mat input_mat;
    eigen2cv(input, input_mat);
    /* Initializing the planes to have both real and imaginary parts */
    Mat padded;
    int m = getOptimalDFTSize( input_mat.rows );
    int n = getOptimalDFTSize( input_mat.cols );
    copyMakeBorder(input_mat, padded, 0, m - input_mat.rows, 0,
                   n - input_mat.cols, BORDER_CONSTANT, Scalar::all(0));
    Mat planes[] = {Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F)};
    /* Computing the DFT */
    merge(planes, 2, output_mat);
    dft(output_mat, output_mat);
}

void InverseFastFourierTransform(Mat input, Eigen::MatrixXf &output) {
    /* Convertion from MatrixXf to Mat */
    Mat output_mat;
    /* Computing the inverse DFT */
    dft(input, output_mat, DFT_INVERSE, DFT_REAL_OUTPUT);

    /* Convertion from Mat to MatrixXf */
    for(int i = 0; i < output_mat.rows; i++) {
        for(int j = 0; j < output_mat.cols; j++) {
            output_mat.at<float>(i, j) = output(i, j);
        }
    }
}

void ConvolutionFFT(cv::Mat &image, cv::Mat &kernel, cv::Mat &output) {
    /* Reallocating the output array if needed */
    output.create(image.rows + kernel.rows - 1, image.cols + kernel.cols - 1,
                  image.type());
    cv::Size dftSize;
    /* Computing the size of DFT transform */
    dftSize.width = cv::getOptimalDFTSize(image.cols + kernel.cols - 1);
    dftSize.height = cv::getOptimalDFTSize(image.rows + kernel.rows - 1);
    /* Allocating temporary buffers and initializing them with zeroes */
    cv::Mat temp_im(dftSize, image.type(), cv::Scalar::all(0));
    cv::Mat temp_ke(dftSize, kernel.type(), cv::Scalar::all(0));
    /* Coping the image and kernel to the top-left corners of temp_im and temp_ke,
    respectively */
    cv::Mat roi_im(temp_im, cv::Rect(0, 0, image.cols, image.rows));
    image.copyTo(roi_im);
    cv::Mat roi_ke(temp_ke, cv::Rect(0, 0, kernel.cols, kernel.rows));
    kernel.copyTo(roi_ke);
    /* Transforming the padded image and kernel in-place,
    using "nonzeroRows" hint for faster processing */
    cv::dft(temp_im, temp_im, 0, image.rows);
    cv::dft(temp_ke, temp_ke, 0, kernel.rows);
    /* Multiplying the spectrums */
    cv::Mat temp_out = cv::Mat::zeros(image.cols, image.rows, CV_32F);
    /* Computing the inverse DFT transformation, keeping only the output.rows */
    cv::idft(temp_im, temp_im, cv::DFT_SCALE, image.rows + kernel.rows - 1);
    /* Copying the result into output */
    temp_im(cv::Rect(0, 0, image.cols, image.rows)).copyTo(output);
}

void BoxBlur(ImageF image, ImageF im_output, string path_save) {
    /* Initializing the kernel */
    MatrixXf kernel = MatrixXf::Constant(7, 7, 1 / 49.);
    /* Getting the images */
    MatrixXf *img = image.GetMatrix();
    MatrixXf *output = im_output.GetMatrix();
    /* Converting to Mat format */
    Mat img_;
    eigen2cv(*img, img_);
    Mat out;
    eigen2cv(*output, out);
    Mat ke;
    eigen2cv(kernel, ke);
    /* Comuting the convolution */
    ConvolutionMatrices(img_, ke, out);
    /* Converting back to Eigen format */
    cv2eigen(out, *output);
    /* Saving the blurred image */
    im_output.Save(path_save);
}

void GaussianBlur(ImageF image, ImageF im_output, string path_save) {
    /* Initializing the kernel */
    MatrixXf kernel;
    kernel.resize(7, 7);
    Point center(3, 3);

    for(int i = 0; i < 7; i++) {
        for(int j = 0; j < 7; j++) {
            kernel(i, j) = exp(-(pow(center.x - i, 2) + \
                                 pow(center.y - j, 2)) / 2.) / (2 * 3.141592);
        }
    }

    /* Getting the images */
    MatrixXf *img = image.GetMatrix();
    MatrixXf *output = im_output.GetMatrix();
    /* Converting to Mat format */
    Mat img_;
    eigen2cv(*img, img_);
    Mat out;
    eigen2cv(*output, out);
    Mat ke;
    eigen2cv(kernel, ke);
    /* Computing the convolution */
    ConvolutionMatrices(img_, ke, out);
    /* Converting back to Eigen format */
    cv2eigen(out, *output);
    /* Saving the blurred image */
    im_output.Save(path_save);
}

void AdaptativeBlur1(ImageF image, ImageF im_output, Point center,
                     string path_save) {
    /* Initializing the radiuses of the sphere (chosen small enough to blur) */
    float big_diameter = 150;
    float small_diameter = 130;
    /* Initializing the identity kernel */
    Mat kernel = (Mat_<float>(5, 5) << 0., 0., 0., 0., 0., 0., 0., 1., 0., 0.,
                  0., 0., 0., 0., 0., 0.);
    /* Getting the images*/
    MatrixXf *img = image.GetMatrix();
    MatrixXf *out = im_output.GetMatrix();
    /* Converting to Mat format */
    Mat matrix_1;
    eigen2cv(*img, matrix_1);
    Mat output;
    eigen2cv(*out, output);
    /* Getting the matrices formats */
    int n = matrix_1.rows;
    int m = matrix_1.cols;
    int p = 5;
    int q = 5;
    /* Initialization of the output matrix */
    output = Mat::zeros(n, m, CV_32F);
    /* Getting the center of the second matrix */
    int kernel_centerx = q / 2;
    int kernel_centery = p / 2;

    /* Going through matrix 1 */
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < m; ++j) {
            /* Computing the distance */
            float r = sqrt(pow((i - center.x) / big_diameter, 2) + \
                           pow((j - center.y) / small_diameter, 2));

            /* Going through matrix 2 */
            for(int k = 0; k < p; ++k) {
                /* keeping the row index to convolve */
                int row_index = p - 1 - k;

                for(int l = 0; l < q; ++l) {
                    /* keeping the column index to convolve */
                    int column_index = q - 1 - l;
                    int new_i = i + (kernel_centery - k);
                    int new_j  = j + (kernel_centerx - l);

                    /* Ignoring out of bounds coefficients */
                    if(new_i >= 0 && new_i < n && new_j >= 0 && new_j < m) {
                        if(r <= 1) {
                            /* Computing the variance*/
                            float v = pow(1 - r, 2);
                            /* Computing the kernel value */
                            float kernel_value = (1 - r) * kernel.at<float>(k, l) +  \
                                                 r * exp(-(pow(kernel_centerx - k, 2) + pow(kernel_centery - l, 2))\
                                                         / (2 * v)) / (2 * 3.141592 * v);
                            output.at<float>(i, j) = output.at<float>(i, j) + \
                                                     matrix_1.at<float>(new_i, new_j) * kernel_value;
                        } else {
                            /* Setting the pixels to white outside of our
                            blurring ellipse */
                            output.at<float>(i, j) = 1.;
                        }

                    }
                }
            }
        }
    }

    cv2eigen(output, *out);
    im_output.Save(path_save);
}

void AdaptativeBlur2(ImageF image, ImageF im_output, Point center,
                     string path_save) {
    /* Initializing the radiuses of the sphere (chosen small enough to blur) */
    float big_diameter = 150;
    float small_diameter = 130;
    /* Initializing the identity kernel */
    Mat kernel = (Mat_<float>(5, 5) << 0., 0., 0., 0., 0., 0., 0., 1., 0., 0.,
                  0., 0., 0., 0., 0., 0.);
    /* Getting the images*/
    MatrixXf *img = image.GetMatrix();
    MatrixXf *out = im_output.GetMatrix();
    /* Converting to Mat format */
    Mat matrix_1;
    eigen2cv(*img, matrix_1);
    Mat output;
    eigen2cv(*out, output);
    /* Getting the matrices formats */
    int n = matrix_1.rows;
    int m = matrix_1.cols;
    int p = 5;
    int q = 5;
    /* Initialization of the output matrix */
    output = Mat::zeros(n, m, CV_32F);
    /* Getting the center of the second matrix */
    int kernel_centerx = q / 2;
    int kernel_centery = p / 2;

    /* Going through matrix 1 */
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < m; ++j) {
            /* Computing the distance */
            float r = sqrt(pow((i - center.x) / big_diameter, 2) + \
                           pow((j - center.y) / small_diameter, 2));

            /* Going through matrix 2 */
            for(int k = 0; k < p; ++k) {
                /* keeping the row index to convolve */
                int row_index = p - 1 - k;

                for(int l = 0; l < q; ++l) {
                    /* keeping the column index to convolve */
                    int column_index = q - 1 - l;
                    int new_i = i + (kernel_centery - k);
                    int new_j  = j + (kernel_centerx - l);

                    /* Ignoring out of bounds coefficients */
                    if(new_i >= 0 && new_i < n && new_j >= 0 && new_j < m) {
                        if(r <= 1) {
                            /* Computing the kernel value */
                            float kernel_value = (1 - r) * kernel.at<float>(k, l) +  \
                                                 r * exp(-(pow(kernel_centerx - k, 2) + pow(kernel_centery - l, 2))\
                                                         / 2.) / (2 * 3.141592);
                            output.at<float>(i, j) = output.at<float>(i, j) + \
                                                     matrix_1.at<float>(new_i, new_j) * kernel_value;
                        } else {
                            /* Setting the pixels to white outside of our
                            blurring ellipse */
                            output.at<float>(i, j) = 1.;
                        }

                    }
                }
            }
        }
    }

    cv2eigen(output, *out);
    im_output.Save(path_save);
}