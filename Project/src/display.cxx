#include "display.h"
#include <typeinfo>
using namespace std;
using namespace cv;

void CallBackFunc(int event, int x, int y, int flags, void *param) {

    // Mat &xyz = *((Mat*)param); //cast and deref the param

    if  ( event == EVENT_LBUTTONDOWN ) {
        Mat *img = reinterpret_cast<Mat *>(param);
        // short val = xyz.at< short >(y,x); // opencv is row-major !

        // circle(*img, Point(x,y), 10, Scalar(0,255,0),3);
        // float val = img[y,x]; // opencv is row-major !
        cout << "Left button of the mouse is clicked - position (" << x << ", "
             << y << ") - Pixel value = " << (*img).at<float>(y, x) << endl;
    } else
        if  ( event == EVENT_RBUTTONDOWN ) {
            Mat *img = reinterpret_cast<Mat *>(param);

            cout << "Right button of the mouse is clicked - position (" << x << ", "
                 << y << ") - Pixel value = " << (*img).at<float>(y, x) << endl;
        } else
            if  ( event == EVENT_MBUTTONDOWN ) {
                Mat *img = reinterpret_cast<Mat *>(param);

                cout << "Middle button of the mouse is clicked - position (" << x << ", "
                     << y << ") - Pixel value = " << (*img).at<float>(y, x) << endl;
            }

    // else if ( event == EVENT_MOUSEMOVE )
    // {
    //     cout << "Mouse move over the window - position (" << x << ", " << y
    //     << ") - Pixel value = " << (*img).at<float>(y,x) << endl;
    // }


}

void DisplayMultipleImages(list<class ImageF> images, string window_name) {
    int size;
    /* Creation of the maximum number of image in a row and in a column */
    int w, h;
    int i, m, n;

    /* Checking the number of arguments and setting the previous values
    accordingly */
    if(images.size() > 12) {
        throw ErrorTooManyImages();
        return;
    } else
        if(images.size() <= 0) {
            throw ErrorNoImages();
            return;
        } else
            if(images.size() == 2) {
                /* 2 images per row and 1 per column */
                h = 1;
                w = 2;
                size = 300;
            } else
                if(images.size() == 3 || images.size() == 4) {
                    /* 2 images per row and column */
                    h = 2;
                    w = 2;
                    size = 300;
                } else
                    if(images.size() == 5 || images.size() == 6) {
                        /* 3 images per row and 2 per column */
                        h = 2;
                        w = 3;
                        size = 200;
                    } else
                        if(images.size() == 7 || images.size() == 8) {
                            /* 4 images per row and 2 per column */
                            h = 2;
                            w = 4;
                            size = 200;
                        } else
                            if(images.size() == 9) {
                                /* 4 images per row and 2 per column */
                                h = 3;
                                w = 3;
                                size = 200;
                            } else {
                                /* 4 images per row and 3 per column */
                                h = 3;
                                w = 4;
                                size = 150;
                            }

    /* Creation of a new image according to the size and the number of images */
    Mat display_image = Mat::zeros(Size(100 + size * w, 60 + size * h), CV_32F);
    /* Loop to fill the new matrix with the images */
    int count = 0 ;
    m = 20;
    n = 20;

    for(auto &i : images) {

        /* Information output */
        string info = "Display image [" + to_string(i.GetHeight()) + ","
                      + to_string(i.GetWidth()) + "] | path: " + i.m_path;

        /* Print in the console */
        cout << info << endl;

        /* Get the width and height of the image, verify if they are equal to
        resize the image */
        int x = i.GetWidth();
        int y = i.GetHeight();
        int max;

        if(x > y) {
            max = x;
        } else
            if(y >= x) {
                max = y;
            }

        /* Find the scale to resize the image */
        float scale = (float) ((float) max / size );

        /* Align the images */
        if(count % w == 0 && m != 20) {
            m = 20;
            n += 20 + size;
        }

        /* Resize of the image to input */
        Mat img;
        eigen2cv(*i.GetMatrix(), img);
        Rect ROI(m, n, (int)( x / scale ), (int)( y / scale ));
        Mat temp;
        resize(img, temp, Size(ROI.width, ROI.height));
        temp.copyTo(display_image(ROI));
        count++ ;
        m += (20 + size);
    }

    /* Change window_name if null */
    if(window_name == "") {
        window_name = "Multiple images output";
    }

    /* Creation of the window to display the iamges */
    namedWindow(window_name, WINDOW_NORMAL);

    //set the callback function for click mouse event
    setMouseCallback(window_name, CallBackFunc, (void *) &display_image);

    imshow(window_name, display_image);
    waitKey(0);
}