// valgrind --leak-check=full --show-leak-kinds=all ./tests/unit_tests/change_value

#include "imagef.h"

using namespace std;
using namespace cv;
using namespace Eigen;

/* Constructors */

ImageF::ImageF(const unsigned int &height, const unsigned int &width) {
    MatrixXd m_imgf(height, width);
}


ImageF::ImageF(Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> mat): m_imgf(
        mat) {}

ImageF::ImageF(string img_path, bool convert_float): m_path(img_path) {

    /* Print in the console */
    cout << "Image construction " << m_path;

    /* We consider all our images in grayscale*/
    Mat imgf_read = imread(img_path, 0);

    /* Image not found */
    if (!imgf_read.data) {
        throw ErrorImageNotFound();
    }

    /* Save the output type wanted by the user */
    m_type = imgf_read.type() & CV_MAT_DEPTH_MASK;

    if (convert_float) {
        m_type = CV_32F;

        /* Print in the console */
        cout << "  - Convert image in float";
    }

    /* Convert float automatically */
    imgf_read.convertTo(imgf_read, CV_32F, 1.f / 255);

    cv2eigen(imgf_read, m_imgf);

    /* Print in the console */
    cout << "   ...done" << endl;

}

ImageF::ImageF(const ImageF &im) {
    m_imgf = im.m_imgf;
    m_path = im.m_path;
    m_type = im.m_type;
}


/* FUNCTIONS*/

void ImageF::DisplayImage(string window_name) const {
    /* Information output */
    string info = "Display image [" + to_string(m_imgf.rows()) + ","
                  + to_string(m_imgf.cols()) + "] | path: " + m_path;

    /* Print in the console */
    cout << info << endl;

    /* Change window_name if null */
    if(window_name == "") {
        window_name = info;
    }

    /* Display the image */
    namedWindow(info, WINDOW_NORMAL);
    Mat imout;
    eigen2cv(m_imgf, imout);

    setMouseCallback(info, CallBackFunc, (void *) &imout);

    imshow(info, imout);
    waitKey(0);
}

float ImageF::Minimum() const {
    return this->m_imgf.minCoeff();
}

float ImageF::Maximum() const {
    return this->m_imgf.maxCoeff();
}

void ImageF::Convert(unsigned char type) {
    /* Wrong input */
    if ((type != CV_8U) && (type != CV_16U) && (type != CV_16S)
            && (type != CV_32S) && (type != CV_32F) && (type != CV_64F)) {
        throw ErrorTypeUnknown();
    }

    m_type = type;

    /* Print in the console */
    cout << "Convert the image to " ;

    switch (m_type) {
    case CV_8U:
        cout << "8U";
        break;

    case CV_8S:
        cout << "8S";
        break;

    case CV_16U:
        cout << "16U";
        break;

    case CV_16S:
        cout << "16S";
        break;

    case CV_32S:
        cout << "32S";
        break;

    case CV_32F:
        cout << "32F";
        break;

    case CV_64F:
        cout << "64F";
        break;

    default:
        throw ErrorTypeUnknown();
        break;
    }

    cout << endl;
}

void ImageF::ChangeValue(Point pixel, float value) {
    /* Position does not exists*/
    if ((pixel.x > m_imgf.rows()) || (pixel.y > m_imgf.cols())) {
        cout << "pixel x " << pixel.x << ", pixel y " << pixel.y << endl;
        throw ErrorOutRange();
    }

    m_imgf(pixel.x, pixel.y) = value;
}

void ImageF::CreateRectangle(Point pixel, unsigned int height,
                             unsigned int width, float value) {
    /* Size overscale image range */
    if ((pixel.x > m_imgf.cols()) || (pixel.y > m_imgf.rows())) {
        throw ErrorOverScale();
    }

    for (unsigned int i = pixel.x; i < pixel.x + width - 1; i++) {
        for (unsigned int j = pixel.y; j < pixel.y + height - 1; j++ ) {
            m_imgf(i, j) = value;
        }
    }
}

void ImageF::AxisSymmetry(unsigned int axis) {
    float tmp_pix;

    if (axis == 1) {
        /* Print in the console */
        cout << "Compute the x-axis symmetry   " ;

        for(int i = 0; i < int(m_imgf.rows()); ++i) {
            for(int j = 0; j < int((m_imgf.cols()) / 2); ++j) {
                tmp_pix = m_imgf(i, j);
                m_imgf(i, j) = m_imgf(i, m_imgf.cols() - j - 1);
                m_imgf(i, m_imgf.cols() - j - 1) = tmp_pix;
            }
        }
    } else
        if (axis == 2) {
            /* Print in the console */
            cout << "Compute the y-axis symmetry   " ;

            for(int i = 0; i < int((m_imgf.rows()) / 2); ++i) {
                for(int j = 0; j < int(m_imgf.cols()); ++j) {
                    tmp_pix = m_imgf(i, j);
                    m_imgf(i, j) = m_imgf(m_imgf.rows() - i - 1, j);
                    m_imgf(m_imgf.rows() - i - 1, j) = tmp_pix;
                }
            }
        } else {
            /* Wrong input*/
            throw ErrorSymmetryInput();
        }

    /* Print in the console */
    cout << "   ...done" << endl;
}

void ImageF::DiagonalSymmetry() {
    AxisSymmetry(1);
    AxisSymmetry(2);
}


void ImageF::Save(string path) {
    Mat img_save;
    eigen2cv(this->m_imgf, img_save);
    img_save = img_save * 255.5f;
    /* Print in the console */
    cout << "Save image   ";

    if (m_type != (img_save.type() & CV_MAT_DEPTH_MASK)) {
        img_save.convertTo(img_save, m_type);

        /* Print in the console */
        cout << "Convert the image to " ;

        switch (img_save.type() & CV_MAT_DEPTH_MASK) {
        case CV_8U:
            cout << "8U";
            break;

        case CV_8S:
            cout << "8S";
            break;

        case CV_16U:
            cout << "16U";
            break;

        case CV_16S:
            cout << "16S";
            break;

        case CV_32S:
            cout << "32S";
            break;

        case CV_32F:
            cout << "32F";
            break;

        case CV_64F:
            cout << "64F";
            break;

        default:
            throw ErrorTypeUnknown();
            break;
        }
    }

    imwrite(path, img_save);
    /* Print in the console */
    cout << " ...done" << endl;

}


void ImageF::SaveAxisSymmetry(unsigned int axis, string path) {
    AxisSymmetry(axis);
    Save(path);
}

void ImageF::SaveDiagonalSymmetry(string path) {
    DiagonalSymmetry();
    Save(path);
}


double CalculateW(double x) {
    double a = -0.1, value, X = std::abs(x);

    if (X == 0) {
        value = 1;
    } else
        if (X <= 1) {
            value = (a + 2) * std::pow(X, 3) - (a + 3) * std::pow(X, 2) + 1;
        } else
            if (X < 2) {
                value = a * std::pow(X, 3) - 5 * a * std::pow(X, 2) + 8 * a * X - 4 * a;
            } else {
                value = 0.0;
            }

    return value;
}

float ImageF::Interpolate(Eigen::Vector3f v)
{
    // // Nearest neighbors
    // return m_imgf(v(0),v(1));

    // Bilinear interpolation
    /***
    a     b
    p
    c     d
    ****/
    Point a = Point(int(v(0)), int(v(1)));
    float a_to_row = v(0) - a.x;
    float a_to_col = v(1) - a.y;
    Point b = Point(a.x, a.y + 1);
    Point c = Point(a.x + 1, a.y);
    Point d = Point(a.x + 1, a.y + 1);

    if (a.x == GetWidth() - 1) {
        c = a;
        d = b;
    }

    if (a.y == GetHeight() - 1) {
        b = a;
        d = c;
    }

    return (1 - a_to_row) * (1 - a_to_col) * m_imgf(a.x, a.y)
                    + (1 - a_to_row) * a_to_col * m_imgf(b.x, b.y)
                    + a_to_row * (1 - a_to_col) * m_imgf(c.x, c.y)
                    + a_to_row * a_to_col * m_imgf(d.x, d.y);


    // // Bicubic inteprolation
    // int tempX = v(0)+0.5, tempY = v(1)+0.5;

    // int arrX[4] = { tempX - 1, tempX, tempX + 1, tempX + 2 };
    // int arrY[4] = { tempY - 1, tempY, tempY + 1, tempY + 2 };



    // if (arrX[0] >= 0 && arrX[3] < GetWidth() && arrY[0] >= 0 && arrY[3] < GetHeight())
    // {
    //     float value = 0;

    //     for (int iteri = 0; iteri < 4; iteri++)
    //     {
    //         for (int iterj = 0; iterj < 4; iterj++)
    //         {
    //             value += (m_imgf(arrX[iteri], arrY[iterj]) * CalculateW(v(0) - arrX[iteri]) * CalculateW(v(1) - arrY[iterj]));
    //         }
    //     }
    //     return value;
    // }
    // else
    // return 1;
}


void ImageF::Translation(float y, float x) {
    Matrix3f t;
    t << 1, 0, x,
    0, 1, -y,
    0, 0, 1;

    Vector3f v(3);
    MatrixXf temp;
    temp = m_imgf;

    for(int i = 0; i < m_imgf.rows(); i++) {
        for(int j = 0; j < m_imgf.cols(); j++) {
            v << i, j, 1;
            v = t * v;

            if ((v(0) > m_imgf.rows() - 1) || (v(1) > m_imgf.cols() - 1)
                    || v(0) < 0 || (v(1) < 0)) {
                temp(i, j) = 1;
            } else {
                temp(i,j) = Interpolate(v);
            }
        }
    }

    m_imgf = temp;
}


void ImageF::Rotation(float r) {
    cv::Point center;
    center.x = m_imgf.rows() / 2;
    center.y = m_imgf.cols() / 2;
    Matrix3f t;
    t << cos(r), sin(r), (1 - cos(r))*center.x - sin(r)*center.y,
    -sin(r), cos(r), sin(r)*center.x + (1 - cos(r))*center.y,
    0, 0, 1;

    Vector3f v(3);
    Point bil;
    MatrixXf temp;
    temp = m_imgf;

    for(int i = 0; i < m_imgf.rows(); i++) {
        for(int j = 0; j < m_imgf.cols(); j++) {
            v << i, j, 1;
            v = t * v;

            if ((v(0) > m_imgf.rows() - 1) || (v(1) > m_imgf.cols() - 1)
                    || v(0) < 0 || (v(1) < 0)) {
                temp(i, j) = 1;
            } else {
                temp(i,j) = Interpolate(v);

            }
        }
    }

    m_imgf = temp;
}
void ImageF::Warp(Eigen::VectorXf p) {
    Rotation(p(2));
    Translation(p(0), p(1));
}

Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>   ImageF::WarpOpenCV(
    float x, float y, float angle) {
    /// def

    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>  m;
    Mat rot_mat( 2, 3, CV_32FC1 );
    Mat warp_mat( 2, 3, CV_32FC1 );
    Mat src, warp_dst, warp_rotate_dst, warpMa;
    //Convertion
    eigen2cv(m_imgf, src);

    warp_dst = Mat::zeros(src.rows, src.cols, src.type());

    warp_mat.at<float>(0, 0) = 1;
    warp_mat.at<float>(1, 0) = 0;
    warp_mat.at<float>(0, 1) = 0;
    warp_mat.at<float>(1, 1) = 1;
    warp_mat.at<float>(0, 2) = x;
    warp_mat.at<float>(1, 2) = y;

    warpAffine( src, warp_dst, warp_mat, warp_dst.size(),
                INTER_LINEAR, BORDER_CONSTANT, Scalar(255, 255, 255));
    Point center = Point( warp_dst.cols / 2, warp_dst.rows / 2 );
    double scale = 1;
    rot_mat = getRotationMatrix2D( center, angle, scale );
    warpAffine( warp_dst, warp_rotate_dst, rot_mat, warp_dst.size(),   INTER_LINEAR,
                BORDER_CONSTANT, Scalar(255, 255, 255));
    cv2eigen(warp_rotate_dst, m);
    return m;
}



/* Getters */
unsigned int ImageF::GetHeight() {
    return m_imgf.rows();
}

unsigned int ImageF::GetWidth() {
    return m_imgf.cols();
}

unsigned char ImageF::GetType() {
    return m_type;
}


Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> *ImageF::GetMatrix() {
    return &this->m_imgf;
}

string ImageF::GetPath() {
    return this->m_path;
}

float ImageF::GetValue(int i, int j) {

    /* Position does not exists*/
    if ((i >= m_imgf.rows()) || (j >= m_imgf.cols()) || (i < 0) || (j < 0)) {
        cout << "pixel x " << i << ", pixel y " << j << endl;
        throw ErrorOutRange();
    }

    return this->m_imgf(i, j);
}
