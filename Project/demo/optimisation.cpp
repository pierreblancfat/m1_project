#include "imagef.h"
#include "optif.h"

using  namespace Eigen;
using  namespace std;


int main(){

    //Initialisation and warp
    ImageF img("../Project/data/clean_finger.png");
    ImageF img2(img.WarpOpenCV(50,50,30));
    OptiF opti(img2, img);
    VectorXf x0(3), h(3);
    h << 1,1,0.01;
    x0 << 1 ,1, 10;


    //NonDifferentiableOptiDescent test
    clock_t begin = clock();
    cout << "Non diff minimum found :" << endl;
    opti.NonDifferentiableOptiDescent(x0,0.1,100);
    cout << "in " << ((double) (clock() - begin)) / CLOCKS_PER_SEC << " sec" << endl << endl;

    //Gradient test
    begin = clock();
    cout << "Gradient descent minimum found :" << endl;
    opti.GradientDescent(x0, h, 0.0000001,0.0000001,100);
    cout << " in " << ((double) (clock() - begin)) / CLOCKS_PER_SEC << " sec" <<  endl <<endl;

}