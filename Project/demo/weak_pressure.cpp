#include "imagef.h"

using namespace std;
using namespace cv;

int main()
{
    ImageF image_test("../Project/data/clean_finger.png");
    Point center = image_test.AreaMinimumIntensity();
    vector<float> pixel_values_1 = image_test.WeakPressure1("../Project/data/clean_finger.png",
    center, "../Project/docs/images/weak_pressure_1.png");
    vector<string> images_path_1;
    images_path_1.push_back("../Project/data/clean_finger.png");
    images_path_1.push_back("../Project/docs/images/weak_pressure_1.png");
    image_test.DisplayMultipleImages("Clean finger and weak pressure finger",2,
    images_path_1);
    vector<float> pixel_values_2 = image_test.WeakPressure2("../Project/data/clean_finger.png",
    center, "../Project/docs/images/weak_pressure_2.png");
    vector<string> images_path_2;
    images_path_2.push_back("../Project/data/clean_finger.png");
    images_path_2.push_back("../Project/docs/images/weak_pressure_2.png");
    image_test.DisplayMultipleImages("Clean finger and weak pressure finger",2,
    images_path_2);
}