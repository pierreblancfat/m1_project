#include "utils_pressure.h"

using namespace std;
using namespace cv;
using namespace Eigen;

int main()
{
    /* Opening the clean_finger image */
    ImageF image_test("../Project/data/clean_finger.png");
    MatrixXf image_characteristics, pixel_values;
    /* Thresholding it */
    Threshold(0.3,"../Project/data/clean_finger.png",
    "./demo/outputs/threshold.png");
    /* Finding the image center */
    ImageCenter("./demo/outputs/threshold.png",
    image_characteristics);
    Point center(image_characteristics(0,0),image_characteristics(0,1));
    /* Applying the isotropic weak pressure simulation */
    IsotropicWeakPressure("../Project/data/clean_finger.png",
    "./demo/outputs/isotropic_weak_pressure.png",center,
    pixel_values);
    /* Applying the anisotropic weak pressure simulation */
    AnisotropicWeakPressure("../Project/data/clean_finger.png",
    "./demo/outputs/anisotropic_weak_pressure.png",center,
    1.5 * image_characteristics(0,2) / 2, 0.5 * image_characteristics(0,3) / 2,
    pixel_values);
}