#include "utils_blur.h"

using namespace std;
using namespace cv;
using namespace Eigen;

int main()
{
    /* Opening the clean finger image */
    ImageF image_in("../Project/data/clean_finger.png");
    ImageF image_out("../Project/data/clean_finger.png");
    MatrixXf image_characteristics, pixel_values;
    /* Thresholding it */
    Threshold(0.3,"../Project/data/clean_finger.png",
    "./demo/outputs/threshold.png");
    /* Finding the image center */
    ImageCenter("./demo/outputs/threshold.png",
    image_characteristics);
    Point center(image_characteristics(0,0),image_characteristics(0,1)-20);
    /* Applying a constant box blur */
    BoxBlur(image_in, image_out, "./demo/outputs/box_blur.png");
    /* Applying a constant gaussian blur */
    GaussianBlur(image_in, image_out, "./demo/outputs/gaussian_blur.png");
    /* Applying an adaptative gaussian blur with a decrease in energy */
    AdaptativeBlur1(image_in, image_out, center,
    "../demo/outputs/adaptative_blur_1.png");
    /* Applying an adaptative gaussian blur with a conservation in energy */
    AdaptativeBlur2(image_in, image_out, center,
    "../demo/outputs/adaptative_blur_2.png");
}