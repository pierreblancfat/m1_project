#include "imagef.h"
#include <math.h>   /* round, floor, ceil, trunc */

using namespace std;
using namespace cv;

int main(int argc, char const *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage " << argv[0] << " [path of the image] "
        << std::endl;
        return EXIT_FAILURE;
    }

    try 
    {
        ImageF image_fgp(argv[1]);
        cout << "Min= " << image_fgp.Minimum();
        cout << "Max= " << image_fgp.Maximum();
        image_fgp.Convert(CV_8U);
        Point pixel(50,50);
        image_fgp.CreateRectangle(pixel,100,100,0);
        Point pixel2(200,200);
        image_fgp.CreateRectangle(pixel2,50,50,1);
        image_fgp.AxisSymmetry(1);
        image_fgp.AxisSymmetry(2);
        image_fgp.Save("./demo/outputs/imagef_basics3.png");
        
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
    catch (ImageF::ErrorTypeUnknown &err)
    {
        std::cerr << err.what() << std::endl;
    }
    catch (ImageF::ErrorOutRange &err)
    {
        std::cerr << err.what() << std::endl;
    }
    catch (ImageF::ErrorOverScale &err)
    {
        std::cerr << err.what() << std::endl;
    }
    catch (ImageF::ErrorSymmetryInput &err)
    {
        std::cerr << err.what() << std::endl;
    }


    return EXIT_SUCCESS;
}