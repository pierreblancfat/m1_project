#include "imagef.h"
#include "display.h"
#include <list>

int main()
{
    try 
    {
        ImageF image_fgp_1("../Project/data/clean_finger.png");
        ImageF image_fgp_2("../Project/data/dry_finger.png");
        ImageF image_fgp_3("../Project/data/moist_finger.png");
        std::list<class ImageF> images_fgp;
        images_fgp.push_back(image_fgp_1);
        images_fgp.push_back(image_fgp_2);
        images_fgp.push_back(image_fgp_3);
        DisplayMultipleImages(images_fgp,"Test DisplayMultipleImages");
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }

    catch (ErrorTooManyImages &err)
    {
        std::cerr << err.what() << std::endl;
    }

    catch (ErrorNoImages &err)
    {
        std::cerr << err.what() << std::endl;
    }
    
    return EXIT_SUCCESS;
}