#include "imagef.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>

TEST(Save, Imgf) {
    try{
        ImageF image_fgp("../Project/data/clean_finger.png");
        image_fgp.Save("tests/unit_tests/outputs/save_clean_finger.png");
        ImageF image_fgp_checked("../Project/data/clean_finger.png");
        EXPECT_EQ(*image_fgp.GetMatrix() , *image_fgp_checked.GetMatrix());
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}