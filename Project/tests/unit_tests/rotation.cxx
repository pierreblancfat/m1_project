#include "imagef.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>
#include <math.h>   /* round, floor, ceil, trunc */

TEST(Rotation, Int)
{
    try
    {
        Eigen::Matrix3f t;
        t << 1, 2, 3,
        4, 5, 6,
        7, 8, 9;
        ImageF imgf(t);
        imgf.Rotation(M_PI/2);
        Eigen::Matrix3f r;
        r << 3, 6, 9,
        2, 5, 8,
        1, 4, 7;
        std::cout << *imgf.GetMatrix();
        EXPECT_EQ(*imgf.GetMatrix(),r);
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}