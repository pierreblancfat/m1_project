#include "imagef.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>

TEST(Convert, Int)
{

    printf("IMPLEMENT CONVERT FONCTION NEEDED");
    try
    {
        ImageF image_fgp("../Project/data/clean_finger.png");
        image_fgp.Convert(CV_16U);
        EXPECT_EQ(CV_16U, image_fgp.GetType());
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}