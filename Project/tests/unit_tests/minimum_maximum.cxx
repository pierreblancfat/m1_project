#include "imagef.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;
using namespace cv;

TEST(Minimum, 0)
{
    try
    {
        ImageF image_fgp("../Project/data/clean_finger.png");
        printf("#########################%f \n",image_fgp.Minimum() );
        EXPECT_EQ(0, image_fgp.Minimum());
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
}

TEST(Maximum, 1)
{
    try
    {
        ImageF image_fgp("../Project/data/clean_finger.png");
        printf("#########################%f \n",image_fgp.Maximum() );
        EXPECT_EQ(1, image_fgp.Maximum());
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
}


int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}