#include "imagef.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>
#include <math.h>   /* round, floor, ceil, trunc */

TEST(Convert, Int)
{
    try
    {
        ImageF image_fgp("../Project/data/clean_finger.png");
        cv::Point pixel(0,0);
        image_fgp.CreateRectangle(pixel,round(image_fgp.GetHeight()/3),
        round(image_fgp.GetWidth()/3),0);
        ImageF image_fgp_checked("../Project/tests/unit_tests/inputs/create_rectangle_clean_finger.png");
        EXPECT_EQ(*image_fgp_checked.GetMatrix(),*image_fgp.GetMatrix());
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}