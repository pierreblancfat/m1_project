#include "imagef.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>

TEST(ChangeValue, 0)
{
    try
    {
        ImageF image_fgp("../Project/data/clean_finger.png");
        cv::Point pixel(0,0);
        image_fgp.ChangeValue(pixel, 0);
        EXPECT_EQ(0, image_fgp.GetValue(0,0));
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}