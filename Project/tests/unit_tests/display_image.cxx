#include "imagef.h"

int main()
{
    try 
    {
        ImageF image_fgp("../Project/data/clean_finger.png", false);
        image_fgp.DisplayImage();
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
    return EXIT_SUCCESS;
}