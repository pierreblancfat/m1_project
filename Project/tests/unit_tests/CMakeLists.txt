file(GLOB SRC_FILES *.cxx)

file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/tests/unit_tests/outputs)

add_executable(display_image_float display_image_float.cxx)
target_link_libraries(display_image_float gtest libFingerPrint ${OpenCV_LIBS}
${Qt_LIBRARIES})
add_test (NAME display_image_float COMMAND display_image_float)

add_executable(display_image display_image.cxx)
target_link_libraries(display_image gtest libFingerPrint ${OpenCV_LIBS})
add_test (NAME display_image COMMAND display_image)

add_executable(display_multiple_images display_multiple_images.cxx)
target_link_libraries(display_multiple_images gtest libFingerPrint ${OpenCV_LIBS})
add_test (NAME display_multiple_images COMMAND display_multiple_images)

add_executable(minimum_maximum minimum_maximum.cxx)
target_link_libraries(minimum_maximum gtest libFingerPrint ${OpenCV_LIBS})
add_test (NAME minimum_maximum COMMAND minimum_maximum)

add_executable(convert convert.cxx)
target_link_libraries(convert gtest libFingerPrint ${OpenCV_LIBS})
add_test (NAME convert COMMAND convert)

add_executable(change_value change_value.cxx)
target_link_libraries(change_value gtest libFingerPrint ${OpenCV_LIBS})
add_test (NAME change_value COMMAND change_value)

add_executable(create_rectangle create_rectangle.cxx)
target_link_libraries(create_rectangle gtest libFingerPrint ${OpenCV_LIBS})
add_test (NAME create_rectangle COMMAND create_rectangle)

add_executable(axis_symmetry axis_symmetry.cxx)
target_link_libraries(axis_symmetry gtest libFingerPrint ${OpenCV_LIBS})
add_test (NAME axis_symmetry COMMAND axis_symmetry)

add_executable(save save.cxx)
target_link_libraries(save gtest libFingerPrint ${OpenCV_LIBS})
add_test (NAME save COMMAND save)

add_executable(translation translation.cxx)
target_link_libraries(translation gtest libFingerPrint ${OpenCV_LIBS})
add_test (NAME translation COMMAND translation)

add_executable(rotation rotation.cxx)
target_link_libraries(rotation gtest libFingerPrint ${OpenCV_LIBS})
add_test (NAME rotation COMMAND rotation)
