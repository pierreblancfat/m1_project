#include "imagef.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>

TEST(SaveAxisSymmetry, X)
{
    try
    {
        ImageF image_fgp("../Project/data/clean_finger.png");
        cv::Point pixel(0,0);
        image_fgp.SaveAxisSymmetry(1, 
            "./tests/unit_tests/outputs/save_symmetry_x_clean_finger.png");
        ImageF image_fgp_checked("../Project/tests/unit_tests/inputs/save_symmetry_x_clean_finger.png");
        EXPECT_TRUE((*image_fgp_checked.GetMatrix() - *image_fgp.GetMatrix()).norm() < 20);
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
}

TEST(SaveAxisSymmetry, Y)
{
    try
    {
        ImageF image_fgp("../Project/data/clean_finger.png");
        cv::Point pixel(0,0);
        image_fgp.SaveAxisSymmetry(2, 
            "./tests/unit_tests/outputs/save_symmetry_x_clean_finger.png");
        ImageF image_fgp_checked("../Project/tests/unit_tests/inputs/save_symmetry_y_clean_finger.png");
        printf("%f \n",(*image_fgp_checked.GetMatrix() - *image_fgp.GetMatrix()).norm());
        EXPECT_TRUE((*image_fgp_checked.GetMatrix() - *image_fgp.GetMatrix()).norm() < 20);
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}