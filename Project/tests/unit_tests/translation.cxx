#include "imagef.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>
#include <math.h>   /* round, floor, ceil, trunc */

TEST(Translation, Int)
{
    try
    {
        Eigen::Matrix3f t;
        t << 1, 2, 3,
        4, 5, 6,
        7, 8, 9;
        ImageF imgf(t);
        imgf.Translation(1,1);
        Eigen::Matrix3f r;
        r << 1, 4, 5,
        1, 7, 8,
        1, 1, 1;
        EXPECT_EQ(*imgf.GetMatrix(),r);
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}