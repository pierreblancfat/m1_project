#include "imagef.h"
#include "optif.h"

using  namespace Eigen;
using  namespace std;
int main(){
    clock_t begin = clock();
    ImageF img("../Project/data/clean_finger.png");
    ImageF img2(img.WarpOpenCV(50,50,-1));
    OptiF opti(img2, img);
    VectorXf x0(3), h(3);
    h << 1,1,0.01;
    x0 << 100 ,30, 10;
    opti.GradientDescent(x0, h, 0.0000001,0.00000001,100);

}