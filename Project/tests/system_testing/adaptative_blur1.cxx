#include "utils_blur.h"

using namespace std;
using namespace cv;
using namespace Eigen;

int main()
{
    ImageF image_in("../Project/data/clean_finger.png");
    ImageF image_out("../Project/data/clean_finger.png");
    Threshold(0.3,"../Project/data/clean_finger.png",
    "./tests/system_testing/outputs/adaptative_blur_1.png");
    MatrixXf characteristics;
    ImageCenter("./tests/system_testing/outputs/adaptative_blur_1.png",
    characteristics);
    Point center(characteristics(0,0),characteristics(0,1)-20);
    AdaptativeBlur1(image_in, image_out, center,
    "./tests/system_testing/outputs/adaptative_blur_1.png");
}