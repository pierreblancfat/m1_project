#include "utils_blur.h"

using namespace std;
using namespace cv;
using namespace Eigen;

int main()
{
    MatrixXf mat_1;
    mat_1.resize(3,3);
    mat_1 << 1., 2., 3., 4., 5., 6., 7., 8., 9.;
    MatrixXf mat_2 = MatrixXf::Zero(3,3);
    mat_2(0,0) = 1;
    MatrixXf output;
    output.Zero(3,3);
    Mat image;
    eigen2cv(mat_1,image);
    Mat kernel;
    eigen2cv(mat_2,kernel);
    Mat out;
    eigen2cv(output,out);
    ConvolutionFFT(image,kernel,out);
    cv2eigen(out,output);
    cout << output << endl;
    ImageF image_test("../Project/data/clean_finger.png");
    MatrixXf *test_image = image_test.GetMatrix();
    Mat test_im;
    eigen2cv(*test_image,test_im);
    MatrixXf *output_2 = image_test.GetMatrix();
    Mat out_2;
    eigen2cv(*output_2,out_2);
    ConvolutionFFT(test_im, kernel, out_2);
    cv2eigen(out_2,*output_2);
    image_test.Save("../Bin/tests/system_testing/outputs/convolution_fft.png");
}