#include "imagef.h"
#include "optif.h"
#include <eigen3/Eigen/Core>

using  namespace Eigen;
using  namespace std;
int main(){
    clock_t begin = clock();
    ImageF img("../Project/data/clean_finger.png");
    VectorXf p(3);
    p << 15, 30, 0;
    ImageF img2(img);
    img2.Warp(p);
    
    OptiF opti(img,img2);
    cout << "GreedyXY: ";
    opti.greedyXY();
    cout << endl << "AllGreedy: ";
    opti.AllGreedy();

    clock_t end = clock();
    double elapsed_secs = double(end-begin) / CLOCKS_PER_SEC;
    cout << elapsed_secs << " seconds" << endl;
}