#include "imagef.h"

int main()
{
    try
    {
        ImageF image_fgp("../Project/data/clean_finger.png");
        image_fgp.SaveAxisSymmetry(1, "./tests/system_testing/outputs/save_axis_symmetry_x_clean_finger.png");
        image_fgp.SaveAxisSymmetry(2, "./tests/system_testing/outputs/save_axis_symmetry_y_clean_finger.png");
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }

    return EXIT_SUCCESS;
}