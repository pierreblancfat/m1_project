#include "utils_blur.h"

using namespace std;
using namespace cv;
using namespace Eigen;

int main()
{
    Mat mat_1 = (Mat_<float>(3,3) << 1., 2., 3., 4., 5., 6., 7., 8., 9.);
    Mat mat_2 = (Mat_<float>(3,3) << 0., 0., 0., 0., 1., 0., 0., 0., 0.);
    Mat output;
    ConvolutionMatrices(mat_1,mat_2, output);
    cout << output << endl;
    ImageF image_test("../Project/data/clean_finger.png");
    MatrixXf *test_image = image_test.GetMatrix();
    MatrixXf *output_2 = image_test.GetMatrix();
    Mat img;
    eigen2cv(*test_image,img);
    Mat out;
    eigen2cv(*output_2,out);
    ConvolutionMatrices(img, mat_2, out);
    cv2eigen(out,*output_2);
    image_test.Save("../Bin/tests/system_testing/outputs/convolution.png");
}