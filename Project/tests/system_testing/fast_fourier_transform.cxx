#include "utils_blur.h"
#include "display.h"

using namespace std;
using namespace cv;
using namespace Eigen;

int main()
{
    ImageF image_test("../Project/data/clean_finger.png");
    ImageF image_transform("../Project/data/clean_finger.png");
    MatrixXf *image = image_test.GetMatrix();
    Mat output;
    MatrixXf *output_2 = image_transform.GetMatrix();
    FastFourierTransform(*image, output);
    InverseFastFourierTransform(output,*output_2);
    image_transform.Save("../Bin/tests/system_testing/outputs/fft.png");
}