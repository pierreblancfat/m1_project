#include "utils_pressure.h"

using namespace std;
using namespace cv;
using namespace Eigen;

int main()
{
    MatrixXf test;
    Threshold(0.3,"../Project/data/clean_finger.png",
    "../Bin/tests/system_testing/outputs/threshold_.png");
    ImageCenter("../Bin/tests/system_testing/outputs/threshold_.png",test);
    Point center(test(0,0),test(0,1));
    ImageF image_test("../Project/data/clean_finger.png");
    image_test.CreateRectangle(center,10,10,1);
    image_test.Save("../Bin/tests/system_testing/outputs/center.png");
    cout << test(0,0) << " " << test(0,1) << " " << test(0,2) << " " 
    << test(0,3) << " " << endl;
}