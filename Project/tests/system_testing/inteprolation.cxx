#include "imagef.h"

using namespace std;
using namespace cv;
using namespace Eigen;

int main()
{
    ImageF img("../Project/data/clean_finger.png");
    ImageF img2(img);
    float angle = M_PI/3;
    img2.Translation(-45,-30);
    img2.Rotation(-0.1);

    std::list<class ImageF> images_fgp;
    images_fgp.push_back(img);
    images_fgp.push_back(img2);
    DisplayMultipleImages(images_fgp,"Test DisplayMultipleImages");
    return 0;
}