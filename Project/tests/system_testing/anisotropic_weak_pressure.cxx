#include "utils_pressure.h"

using namespace std;
using namespace cv;
using namespace Eigen;

int main()
{
    MatrixXf image_characteristics, pixel_values;
    Threshold(0.3,"../Project/data/clean_finger.png",
    "../Bin/tests/system_testing/outputs/threshold_anisotropic.png");
    ImageCenter("../Bin/tests/system_testing/outputs/threshold_anisotropic.png",
    image_characteristics);
    Point center(image_characteristics(0,0),image_characteristics(0,1));
    AnisotropicWeakPressure("../Project/data/clean_finger.png",
    "../Bin/tests/system_testing/outputs/anisotropic_weak_pressure.png",center,
    1.5 * image_characteristics(0,2) / 2, 0.5 * image_characteristics(0,3) / 2,
    pixel_values);
}