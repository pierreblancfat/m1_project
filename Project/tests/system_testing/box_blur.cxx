#include "utils_blur.h"

using namespace std;
using namespace cv;
using namespace Eigen;

int main()
{
    ImageF image_in("../Project/data/clean_finger.png");
    ImageF image_out("../Project/data/clean_finger.png");
    BoxBlur(image_in, image_out, 
    "../Bin/tests/system_testing/outputs/box_blur.png");
}