#include "imagef.h"

using namespace std;
using namespace cv;

int main()
{
    ImageF image_fgp("../Project/data/clean_finger.png");
    Point arg_min = image_fgp.AreaMinimumIntensity();
    cout << "Pixel with minimum intensity around is : " << arg_min << endl;

    Point arg_max = image_fgp.AreaMaximumIntensity();
    cout << "Pixel with maximal intensity around is : " << arg_max << endl;

}