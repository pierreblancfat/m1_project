#include "utils_pressure.h"

using namespace std;
using namespace cv;
using namespace Eigen;

int main()
{
    ImageF image_test("../Project/data/clean_finger.png");
    MatrixXf image_characteristics, pixel_values;
    Threshold(0.3,"../Project/data/clean_finger.png",
    "../Bin/tests/system_testing/outputs/threshold_isotropic.png");
    ImageCenter("../Bin/tests/system_testing/outputs/threshold_isotropic.png",
    image_characteristics);
    Point center(image_characteristics(0,0),image_characteristics(0,1));
    IsotropicWeakPressure("../Project/data/clean_finger.png",
    "../Bin/tests/system_testing/outputs/isotropic_weak_pressure.png",center,
    pixel_values);
}