#include "imagef.h"
#include "optif.h"
#include <eigen3/Eigen/Core>

using  namespace Eigen;
using  namespace std;
int main(){
    clock_t begin = clock();
    ImageF img("../Project/data/clean_finger.png");
    ImageF img2(img.WarpOpenCV(50,50,-1));
    OptiF opti(img2, img);
    VectorXf x0(3), h(3);
    h << 1,1,0.01;
    x0 << 100 ,30, 10;
    opti.NonDifferentiableOptiDescent(x0, 0.1, 100);

}