#include "imagef.h"

int main()
{
    try
    {
        ImageF image_fgp("../Project/data/clean_finger.png");
        image_fgp.SaveDiagonalSymmetry("./tests/system_testing/outputs/save_diagonal_symmetry_clean_finger.png");
    }

    catch (ImageF::ErrorImageNotFound &err)
    {
        std::cerr << err.what() << std::endl;
    }

    return EXIT_SUCCESS;
}